# C11 Generics #

A simple implementation of type generic bounds-checked arrays in C, using the
C11 \_Generic keyword and XX list macros.

Compile with a C11-compliant compiler providing \_Generic keyword support using:   
`cc -std=c11 -O3 chkd_array_demo.c -o chkd_array_demo`

Annotate and view the output of the included performance profiles using:      
`callgrind_annotate callgrind.out.xxxxx`   
`cachegrind_annotate cachegrind.out.xxxxx`

Run your own performance profiles (assuming you have Valgrind) using:   
`valgrind --dsymutil=yes --tool=callgrind ./chkd_array_demo 50000000 50000 500`   
`valgrind --dsymutil=yes --tool=cachegrind --branch-sim=yes ./chkd_array_demo 50000000 50000 500`   
(Tweak the last 3 numbers on both command lines to vary the test params.)

My system architecture:   
`AMD Phenom(tm) II X4 955 Processor, 512KB cache`   
`Linux/Debian version: 3.10-2-amd64 #1 SMP Debian 3.10.5-1 (2013-08-07) x86_64 GNU/Linux`   
`Debian clang version 3.5-1~exp1 (trunk) (based on LLVM 3.5), Target: x86_64-pc-linux-gnu`   
`Valgrind version 3.8.1`

Some inspiration and education taken from:    
[http://www.robertgamble.net/2012/01/c11-generic-selections.html](http://www.robertgamble.net/2012/01/c11-generic-selections.html)   
[http://stackoverflow.com/questions/10950828/simulation-of-templates-in-c](http://stackoverflow.com/questions/10950828/simulation-of-templates-in-c)
[http://stackoverflow.com/questions/147267/easy-way-to-use-variables-of-enum-types-as-string-in-c/202511#202511](http://stackoverflow.com/questions/147267/easy-way-to-use-variables-of-enum-types-as-string-in-c/202511#202511)

See the long-form discussion at:
[http://abissell.com/2014/01/16/c11s-_generic-keyword-macro-applications-and-performance-impacts/](http://abissell.com/2014/01/16/c11s-_generic-keyword-macro-applications-and-performance-impacts/)