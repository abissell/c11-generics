// Guards to ensure lists of types for which to create defs are defined
#ifndef CHKD_ARRAY_TYPES
#error "CHKD_ARRAY_TYPES not defined for defs"
#endif
#ifndef CHKD_ARRAY_PRIMITIVE_TYPES
#error "CHKD_ARRAY_PRIMITIVE_TYPES not defined for defs"
#endif

DECLARE(CHKD_ARRAY_TYPES)

// Definition of implementations for the generic functions.
// Note that we cannot use a generic macro to define the IS_EQUAL impl
//      and must instead define it for each subtype.
#define CHKD_ARRAY_DEF(T, T_NM, DESC_LIT)                                      \
                                                                               \
    static CHKD_ARRAY(T_NM) * CREATE(T_NM)(unsigned length, T *err_val)        \
    {                                                                          \
        T *data = (T *)calloc(length, sizeof(T));                              \
        if (data == NULL) {                                                    \
            return NULL;                                                       \
        }                                                                      \
                                                                               \
        CHKD_ARRAY(T_NM) *chkd_array =                                         \
                (CHKD_ARRAY(T_NM) *)malloc(sizeof(CHKD_ARRAY(T_NM)));          \
        if (chkd_array == NULL) {                                              \
            free(data);                                                        \
            return NULL;                                                       \
        }                                                                      \
                                                                               \
        chkd_array->err_val = *err_val;                                        \
                                                                               \
        T *err_check = (T *)calloc(1, sizeof(T));                              \
        if (err_check == NULL) {                                               \
            free(data);                                                        \
            free(chkd_array);                                                  \
            return NULL;                                                       \
        }                                                                      \
                                                                               \
        if (IS_EQUAL(T_NM)(&chkd_array->err_val, err_check)) {                 \
            free(err_check);                                                   \
            free(data);                                                        \
            free(chkd_array);                                                  \
            return NULL;                                                       \
        }                                                                      \
                                                                               \
        free(err_check);                                                       \
        chkd_array->data = data;                                               \
        chkd_array->length = length;                                           \
                                                                               \
        return chkd_array;                                                     \
    }                                                                          \
                                                                               \
    static inline T SET(T_NM)(CHKD_ARRAY(T_NM) * self, unsigned idx, T *val)   \
    {                                                                          \
        if (IS_EQUAL(T_NM)(&self->err_val, val) || idx >= self->length) {      \
            return self->err_val;                                              \
        }                                                                      \
                                                                               \
        T *data = self->data;                                                  \
        T old_val = data[idx];                                                 \
        data[idx] = *val;                                                      \
                                                                               \
        return old_val;                                                        \
    }                                                                          \
                                                                               \
    static inline T *GET(T_NM)(CHKD_ARRAY(T_NM) * self, unsigned idx)          \
    {                                                                          \
        if (idx >= self->length) {                                             \
            return &self->err_val;                                             \
        }                                                                      \
                                                                               \
        return &self->data[idx];                                               \
    }                                                                          \
                                                                               \
    static inline char *DESC(T_NM)() { return (DESC_LIT); }

// Definition of IS_EQUAL implementations for types using == operator
#define IS_EQUAL_DEF(T, T_NM, DESC_LIT)                                        \
                                                                               \
    static inline bool IS_EQUAL(T_NM)(T *first, T *second)                     \
    {                                                                          \
        return *first == *second;                                              \
    }

// Define the checked array methods for the passed-in types
#define DEFINE(TYPES) TYPES(CHKD_ARRAY_DEF)
DEFINE(CHKD_ARRAY_TYPES)

// Define the == is_equal method for the passed-in primitive types
#define DEF_PRIMITIVES(TYPES) TYPES(IS_EQUAL_DEF)
DEF_PRIMITIVES(CHKD_ARRAY_PRIMITIVE_TYPES)

#define CHKD_ARRAY_CREATE(length, T_PTR)                                       \
    GENERIC_VAL_PTR_FN_DEFINE((T_PTR), create)(length, (T_PTR))

#define CHKD_ARRAY_IS_EQUAL(T_PTR, SECOND_PTR)                                 \
    GENERIC_VAL_PTR_FN_DEFINE((T_PTR), is_equal)((T_PTR), (SECOND_PTR))

#define CHKD_ARRAY_SET(CHKD_ARRAY_PTR, idx, T)                                 \
    GENERIC_ARRAY_PTR_FN_DEFINE((CHKD_ARRAY_PTR), set)((CHKD_ARRAY_PTR), idx,  \
                                                       (T))

#define CHKD_ARRAY_GET(CHKD_ARRAY_PTR, idx)                                    \
    GENERIC_ARRAY_PTR_FN_DEFINE((CHKD_ARRAY_PTR), get)((CHKD_ARRAY_PTR), idx)

#define CHKD_ARRAY_DESC(CHKD_ARRAY_PTR)                                        \
    GENERIC_ARRAY_PTR_FN_DEFINE((CHKD_ARRAY_PTR), desc)()
