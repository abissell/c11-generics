// Primary definitions of the chkd_array data structure and functions on it

// Guards to ensure we don't reuse an old list of types
#ifdef CHKD_ARRAY_TYPES
#error "CHKD_ARRAY_TYPES defined before included header"
#endif
#ifdef CHKD_ARRAY_PRIMITIVE_TYPES
#error "CHKD_ARRAY_PRIMITIVE_TYPES defined before included header"
#endif

#include <stdbool.h>
#include <stdlib.h>
#include "../generic-lib/generic_defs.h"

// Expansion macro for checked array struct of given type T.
#define CHKD_ARRAY(T_NM) TYPED_STRUCT(chkd_array, T_NM)
// Expansion macros for functions on checked arrays of given type T.
#define CREATE(T_NM) TYPED_FN(chkd_array, T_NM, create)
#define IS_EQUAL(T_NM) TYPED_FN(chkd_array, T_NM, is_equal)
#define SET(T_NM) TYPED_FN(chkd_array, T_NM, set)
#define GET(T_NM) TYPED_FN(chkd_array, T_NM, get)
#define DESC(T_NM) TYPED_FN(chkd_array, T_NM, desc)

// Declaration of the checked array typed struct and methods.
#define CHKD_ARRAY_DEC(T, T_NM, DESC_LIT)                                      \
                                                                               \
    CHKD_ARRAY(T_NM)                                                           \
    {                                                                          \
        T *data;                                                               \
        unsigned length;                                                       \
        T err_val;                                                             \
    };                                                                         \
    static CHKD_ARRAY(T_NM) * CREATE(T_NM)(unsigned length, T *err_val);       \
    static inline bool IS_EQUAL(T_NM)(T *first, T *second);                    \
    static inline T SET(T_NM)(CHKD_ARRAY(T_NM) * self, unsigned idx, T *val);  \
    static inline T *GET(T_NM)(CHKD_ARRAY(T_NM) * self, unsigned idx);         \
    static inline char *DESC(T_NM)();

// Declare the checked array for the passed-in types ALL_TYPES.
#define DECLARE(ALL_TYPES) ALL_TYPES(CHKD_ARRAY_DEC)
