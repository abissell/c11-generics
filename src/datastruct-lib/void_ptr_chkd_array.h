#include <stdbool.h>
#include <string.h>

struct vpa {
    void *data;
    unsigned length;
    void *err_ptr;
    bool (* is_equal)(void *first, void *second);
    void *(*set)(struct vpa *self, unsigned idx, void *val);
    void *(*get)(struct vpa *self, unsigned idx);
    char *desc;
};

static inline bool is_equal_int(int *first, int *second)
{
    return *first == *second;
}

static inline bool is_equal_float(float *first, float *second)
{
    return *first == *second;
}

static inline int *set_int(struct vpa *self, unsigned idx, int *val)
{
    void *const err_ptr = self->err_ptr;
    if (self->is_equal(val, err_ptr) || idx >= self->length) {
        return (int *) err_ptr;
    }

    int *int_data = (int *)self->data;
    int_data[idx] = *val;
    return &int_data[idx];
}

static inline float *set_float(struct vpa *self, unsigned idx, float *val)
{
    void *const err_ptr = self->err_ptr;
    if (self->is_equal(val, err_ptr) || idx >= self->length) {
        return (float *) err_ptr;
    }

    float *float_data = (float *)self->data;
    float_data[idx] = *val;
    return &float_data[idx];
}

static inline int *get_int(struct vpa *self, unsigned idx)
{
    if (idx >= self->length) {
        return (int *) self->err_ptr;
    }

    int *int_data = (int *)self->data;
    return &int_data[idx];
}

static inline float *get_float(struct vpa *self, unsigned idx)
{
    if (idx >= self->length) {
        return (float *) self->err_ptr;
    }

    float *float_data = (float *)self->data;
    return &float_data[idx];
}

static struct vpa *create_vpa(unsigned length, void *err_ptr,
                              bool (*is_equal)(void *, void *),
                              void *(*set)(struct vpa *, unsigned, void *),
                              void *(*get)(struct vpa *, unsigned), char *desc,
                              size_t elem_size)
{
    void *data = NULL;
    struct vpa *vpa_ptr = NULL;
    void *dup_err_ptr = NULL;
    void *err_check = NULL;

    data = (void *)calloc(length, elem_size);
    if (data == NULL) goto cleanup;

    vpa_ptr = malloc(sizeof(struct vpa));
    if (vpa_ptr == NULL) goto cleanup;

    // Allocate separate memory for the err_ptr value
    dup_err_ptr = malloc(elem_size);
    if (dup_err_ptr == NULL) goto cleanup;

    memcpy(dup_err_ptr, err_ptr, elem_size);
    vpa_ptr->err_ptr = dup_err_ptr;

    // Double-check that error value is not equal to zeroed memory
    err_check = calloc(1, elem_size);
    if (err_check == NULL) goto cleanup;

    // !! Goto cleanup here if err_ptr == err_check

    free(err_check);
    vpa_ptr->data = data;
    vpa_ptr->length = length;
    vpa_ptr->is_equal = is_equal;
    vpa_ptr->set = set;
    vpa_ptr->get = get;
    vpa_ptr->desc = desc;

    return vpa_ptr;

cleanup:
    if (data != NULL) free(data);
    if (dup_err_ptr != NULL) free(dup_err_ptr);
    if (vpa_ptr != NULL) free(vpa_ptr);
    if (err_check != NULL) free(err_check);

    return NULL;
}
