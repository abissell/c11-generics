// Some simple definitions used in generic type factory header files

// Concatenate names of typed structs and functions with the type param
#define TYPED_STRUCT(NAME, T) struct NAME##_##T
#define TYPED_FN(NAME, T, FN_NAME) NAME ##_##T##_##FN_NAME
