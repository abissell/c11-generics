// Demonstrates the use of the generic bounds-checked arrays
// set up in chkd_array_demo.h

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <float.h>
#include "datastruct-lib/void_ptr_chkd_array.h"
#include "chkd_array_demo.h"

// Since == cannot be used in the def of is_equal() for strct, define it here
static inline bool chkd_array_strct_is_equal(strct *first, strct *second)
{
    return first->a == second->a && first->b == second->b;
}

int rand_int;
float rand_float;
strct rand_strct;

// Provide means to generate random values of ints, floats, and strcts
static inline strct gen_strct_val()
{
    strct gen = { .a=rand(), .b=(float) rand() };
    return gen;
}

// Set up some storage for random values of different types for chkd_arrays
enum { RAND_VALS = 100000 };
int rand_ints[RAND_VALS];
float rand_floats[RAND_VALS];
strct rand_strcts[RAND_VALS];

enum type {
    INT,
    FLOAT,
    STRCT
};

// Return an array filled with a random sequence of int, float or
// strct types (for randomizing function calls)
static enum type *get_random_types(unsigned num_types, unsigned chunk_size)
{
    enum type *const types = malloc(num_types * sizeof(enum type));
    for (unsigned i = 0; i < num_types;) {
        const int rand_type = rand() % 3;
        for (unsigned j = 0; j < chunk_size; ++j) {
            types[i++] = rand_type;
            if (i == num_types) break;
        }
    }

    return types;
}

// Generic macro for random value array of given type
#define RAND_VAL(CHKD_ARRAY_PTR, idx)                                          \
  _Generic( (CHKD_ARRAY_PTR), \
          chkd_array_int *: rand_ints[idx], \
          chkd_array_float *: rand_floats[idx], \
          chkd_array_strct *: rand_strcts[idx])

// Generic macro for random constants of given type
#define RAND_CONST(T)                                                          \
  _Generic( (T), \
          chkd_array_int *: rand_int,\
          chkd_array_float *: rand_float, \
          chkd_array_strct *: rand_strct)

// Generic macro for generating random values of given type
#define GEN_VAL(CHKD_ARRAY_PTR)                                                \
  _Generic( (CHKD_ARRAY_PTR), \
        chkd_array_int *: rand(), \
        chkd_array_float *: (float) rand(),\
        chkd_array_strct *: gen_strct_val())

// Generic macro for generating RAND_VALS random values, using GEN_VAL macro
#define GEN_RAND_VALS(CHKD_ARRAY_PTR)                                          \
  for (unsigned i = 0; i < RAND_VALS; ++i) { \
      _Generic( (CHKD_ARRAY_PTR),\
                chkd_array_int *: rand_ints[i],\
                chkd_array_float *: rand_floats[i],\
                chkd_array_strct *: rand_strcts[i]) = GEN_VAL(CHKD_ARRAY_PTR); \
  }

// Declare and define generic function which writes and compares random values
// Reads num_vals random values, sets them in chkd_array, and checks each one
// against a random value, printing a message to stdout if there is a match
#define WRITE_VALS(CHKD_ARRAY_PTR) TYPED_FN(write, CHKD_ARRAY_PTR, vals)
#define WRITE_VALS_DEF(CHKD_ARRAY_PTR)                                         \
    static inline void WRITE_VALS(CHKD_ARRAY_PTR)(                             \
            CHKD_ARRAY_PTR *chkd_array_ptr, unsigned num_vals,                 \
            unsigned rand_start, unsigned array_start)                         \
    {                                                                          \
        const unsigned chkd_array_len = chkd_array_ptr->length;                \
        unsigned array_idx = array_start;                                      \
        for (unsigned i = 0; i < num_vals; ++i) {                              \
            unsigned write_idx = array_idx++ % chkd_array_len;                 \
            unsigned read_idx = (rand_start + i) % RAND_VALS;                  \
            CHKD_ARRAY_SET(chkd_array_ptr, write_idx,                          \
                           &RAND_VAL(chkd_array_ptr, read_idx));               \
            if (!CHKD_ARRAY_IS_EQUAL(CHKD_ARRAY_GET(chkd_array_ptr, write_idx),\
                        &RAND_VAL(chkd_array_ptr, read_idx))) {                \
                printf("Could not retrieve recently set val!\n");              \
            }                                                                  \
            if (CHKD_ARRAY_IS_EQUAL(CHKD_ARRAY_GET(chkd_array_ptr, write_idx), \
                                    &RAND_CONST(chkd_array_ptr))) {            \
                printf("An amazing match! %s\n",                               \
                       CHKD_ARRAY_DESC(chkd_array_ptr));                       \
            }                                                                  \
        }                                                                      \
    }

WRITE_VALS_DEF(chkd_array_int)
WRITE_VALS_DEF(chkd_array_float)
WRITE_VALS_DEF(chkd_array_strct)

// void ptr version of the read-set-check-print function above
static inline void write_vpa_vals(vpa *vpa, unsigned num_vals,
        unsigned rand_start, unsigned array_start, void *rand_vals,
        void *rand_const)
{
    const unsigned vpa_len = vpa->length;
    const size_t elem_size = vpa->elem_size;
    unsigned array_idx = array_start;
    for (unsigned i = 0; i < num_vals; ++i) {
        unsigned write_idx = array_idx++ % vpa_len;
        unsigned read_idx = (rand_start + i) % RAND_VALS;
        void *rand_vals_pos = rand_vals + read_idx*elem_size;
        vpa_set(vpa, write_idx, rand_vals_pos);
        if (!vpa_is_equal(vpa_get(vpa, write_idx), rand_vals_pos, elem_size)) {
            printf("vpa could not retrieve recently set val!\n");
        }
        if (vpa_is_equal(vpa_get(vpa, write_idx), rand_const, elem_size)) {
            printf("An amazing vpa match! %s\n", vpa_desc());
        }
    }
}

enum { LENGTH = 128 };

// Runs the generic WRITE_VALS macro for n splits, using the input rand_types
// to decide which type to run
void make_generic_calls(unsigned splits, enum type *rand_types,
        unsigned its_per_split, CHKD_ARRAY(int) *int_array,
        CHKD_ARRAY(float) *float_array, CHKD_ARRAY(strct) *strct_array)
{
    unsigned rand_start = 0, array_start = 0;

    for (unsigned i = 0; i < splits; ++i) {
        enum type split_type = rand_types[i];
        switch (split_type) {
            case INT:
                WRITE_VALS(chkd_array_int)(int_array, its_per_split, rand_start,
                                           array_start);
                break;
            case FLOAT:
                WRITE_VALS(chkd_array_float)(float_array, its_per_split,
                        rand_start, array_start);
                break;
            case STRCT:
                WRITE_VALS(chkd_array_strct)(strct_array, its_per_split,
                        rand_start, array_start);
                break;
        }

        rand_start = (rand_start + its_per_split) % RAND_VALS;
        array_start = (array_start + its_per_split) % LENGTH;
   }
}

// void ptr version of the function above
void make_vpa_calls(unsigned splits, enum type *rand_types,
        unsigned its_per_split, vpa *int_vpa, vpa *float_vpa, vpa *strct_vpa)
{
    unsigned rand_start = 0, array_start = 0;

    for (unsigned i = 0; i < splits; ++i) {
        enum type split_type = rand_types[i];
        switch (split_type) {
            case INT:
                write_vpa_vals(int_vpa, its_per_split, rand_start, array_start,
                        rand_ints, &rand_int);
                break;
            case FLOAT:
                write_vpa_vals(float_vpa, its_per_split, rand_start,
                        array_start, rand_floats, &rand_float);
                break;
            case STRCT:
                write_vpa_vals(strct_vpa, its_per_split, rand_start,
                        array_start, rand_strcts, &rand_strct);
                break;
        }

        rand_start = (rand_start + its_per_split) % RAND_VALS;
        array_start = (array_start + its_per_split) % LENGTH;
    }
}

int main(int argc, char *argv[])
{
    if (argc != 4) {
        printf("Usage: %s iterations splits chunk_size\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    const unsigned iterations = (unsigned int) atoi(argv[1]);
    const unsigned splits = (unsigned int) atoi(argv[2]);
    if (splits > iterations) {
        printf("ERROR: Can't divide %d splits into %d iterations\n",
                splits, iterations);
    }

    const unsigned its_per_split = iterations / splits;
    const unsigned chunk_size = (unsigned int) atoi(argv[3]);
    printf("Runnings %d iterations in %d splits, %d iterations per split, "
            "%d same-type consecutive split calls\n\n",
            iterations, splits, its_per_split, chunk_size);

    // Seed the PRNG and set the rand constants
    srand(time(0));
    rand_int = rand();
    rand_float = (float) rand();
    rand_strct = gen_strct_val();

    // Define constant err values for the different types
    int err_int = INT_MIN;
    float err_float = FLT_MIN;
    strct err_strct = { .a=err_int, .b=err_float};

    // Set up int, float, and strct type arrays
    CHKD_ARRAY(int) *int_array = CHKD_ARRAY_CREATE(LENGTH, &err_int);
    if (int_array == NULL) {
        printf("ERROR: Bad err_int initialization to %d\n", err_int);
        exit(1);
    }

    CHKD_ARRAY(float) *float_array = CHKD_ARRAY_CREATE(LENGTH, &err_float);
    if (float_array == NULL) {
        printf("ERROR: Bad err_float initialization to %f\n", err_float);
        exit(1);
    }

    CHKD_ARRAY(strct) *strct_array = CHKD_ARRAY_CREATE(LENGTH, &err_strct);
    if (strct_array == NULL) {
        printf("ERROR: Bad err_strct initialization\n");
        exit(1);
    }

    // Set up void ptr versions of int, float, and strct type arrays
    vpa *int_vpa = create_vpa(LENGTH, &err_int, sizeof(int));
    vpa *float_vpa = create_vpa(LENGTH, &err_float, sizeof(float));
    vpa *strct_vpa = create_vpa(LENGTH, &err_strct, sizeof(strct));

    // Generate statically alloc'd random values for each of the different types
    GEN_RAND_VALS(int_array);
    GEN_RAND_VALS(float_array);
    GEN_RAND_VALS(strct_array);

    // Generate random order in which to call functions on the different types
    enum type *const rand_types = get_random_types(splits, chunk_size);

    clock_t gen_begin, gen_end, vpa_begin, vpa_end;
    double gen_time_spent, vpa_time_spent;

    // Run the generic version of the read-set-check-print functions
    gen_begin = clock();
    make_generic_calls(splits, rand_types, its_per_split, int_array,
            float_array, strct_array);
    gen_end = clock();
    gen_time_spent = (double)(gen_end - gen_begin) / CLOCKS_PER_SEC;

    // Run the void ptr version of the read-set-check-print functions
    vpa_begin = clock();
    make_vpa_calls(splits, rand_types, its_per_split, int_vpa, float_vpa,
            strct_vpa);
    vpa_end = clock();
    vpa_time_spent = (double)(vpa_end - vpa_begin) / CLOCKS_PER_SEC;

    printf("generic time spent: %f\n", gen_time_spent);
    printf("void ptr time spent: %f\n", vpa_time_spent);
}
