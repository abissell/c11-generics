#include <stdbool.h>
#include <string.h>

typedef struct {
    void *data;
    unsigned length;
    void *err_ptr;
    size_t elem_size;
} vpa;

static inline bool vpa_is_equal(void *first, void *second, size_t elem_size)
{
    int res = memcmp(first, second, elem_size);
    return res == 0;
}

static vpa *create_vpa(unsigned length, void *err_ptr, size_t elem_size)
{
    void *data = NULL;
    vpa *vpa_ptr = NULL;
    void *dup_err_ptr = NULL;
    void *err_check = NULL;

    data = (void *)calloc(length, elem_size);
    if (data == NULL) goto cleanup;

    vpa_ptr = malloc(sizeof(vpa));
    if (vpa_ptr == NULL) goto cleanup;

    // Allocate separate memory for the err_ptr value
    dup_err_ptr = malloc(elem_size);
    if (dup_err_ptr == NULL) goto cleanup;

    memcpy(dup_err_ptr, err_ptr, elem_size);
    vpa_ptr->err_ptr = dup_err_ptr;

    // Double-check that error value is not equal to zeroed memory
    err_check = calloc(1, elem_size);
    if (err_check == NULL) goto cleanup;

    if (vpa_is_equal(vpa_ptr->err_ptr, err_check, elem_size)) goto cleanup;

    free(err_check);
    vpa_ptr->data = data;
    vpa_ptr->length = length;
    vpa_ptr->elem_size = elem_size;

    return vpa_ptr;

cleanup:
    if (data != NULL) free(data);
    if (dup_err_ptr != NULL) free(dup_err_ptr);
    if (vpa_ptr != NULL) free(vpa_ptr);
    if (err_check != NULL) free(err_check);

    return NULL;
}

static inline void *vpa_set(vpa *self, unsigned idx, void *val)
{
    const size_t elem_size = self->elem_size;
    if (vpa_is_equal(self->err_ptr, val, elem_size)
            || idx >= self->length) {
        return self->err_ptr;
    }

    void *data_pos = self->data + idx*elem_size;
    memcpy(data_pos, val, elem_size);
    return val;
}

static inline void *vpa_get(vpa *self, unsigned idx)
{
    if (idx >= self->length) {
        return self->err_ptr;
    }

    return self->data + idx * self->elem_size;
}

static inline char *vpa_desc()
{
    return "void_ptr_array";
}
