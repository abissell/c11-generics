#include "datastruct-lib/chkd_array_factory_head.h"

// Add a new local type named "strct" before defining generic functions
struct strct {
    int a;
    float b;
};

// Define the lists for which to create the checked array defs and decls
#define CHKD_ARRAY_PRIMITIVE_TYPES(XX) \
    XX(int, int, "chkd_array_int") \
    XX(float, float, "chkd_array_float")

#define CHKD_ARRAY_TYPES(XX) \
    CHKD_ARRAY_PRIMITIVE_TYPES(XX) \
    XX(struct strct, struct_strct, "chkd_array_struct_strct")

// Create the generic function names for val ptr functions
#define GENERIC_VAL_PTR_FN_DEFINE(T_PTR, FN_NAME)                             \
    _Generic((T_PTR), \
                int *: (chkd_array_int ## _ ## FN_NAME), \
                float *: (chkd_array_float ## _ ## FN_NAME), \
                struct strct *: (chkd_array_struct_strct ## _ ## FN_NAME))

// Create the generic function names for chkd_array ptr functions
#define GENERIC_ARRAY_PTR_FN_DEFINE(CHKD_ARRAY_PTR, FN_NAME)                   \
    _Generic((CHKD_ARRAY_PTR), \
                struct chkd_array_int *: (chkd_array_int ## _ ## FN_NAME), \
                struct chkd_array_float *: (chkd_array_float ## _ ## FN_NAME), \
                struct chkd_array_struct_strct *: (chkd_array_struct_strct ## _ ## FN_NAME))

// Declares and defines the generic val ptr and chkd_array ptr functions
#include "datastruct-lib/chkd_array_factory_defs.h"
